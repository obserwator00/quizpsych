import {getQuestions} from './questions.js';
import {showResult} from './result.js';

function loadButtons(btnsStr, infoQuestion, answers){
    let htmlButtons = []
    btnsStr.forEach((element) =>{
        let btn = document.getElementsByClassName(element).item(0);
        btn.addEventListener('click', () => {
            makeAnswer(btn, htmlButtons, infoQuestion, answers)
            checkIfAllAnswered(infoQuestion, answers);
        })
        htmlButtons.push(btn);
    })

    return htmlButtons;
}

function checkIfAllAnswered(infoQuestion, answers){
    if(infoQuestion.questions.length === answers.length){
        let finishButton = document.getElementsByClassName('finish-test').item(0);
        finishButton.style.display = "block";
        finishButton.addEventListener('click', () => {
            let wrapper = document.getElementsByClassName('wrapper').item(0);
            for(let child of wrapper.childNodes){
                if(child.className){
                    if(child.className !== 'my-dataviz')
                        document.getElementsByClassName(child.className).item(0).style.display = 'none';
                     else 
                        document.getElementsByClassName(child.className).item(0).style.display = 'block';
                }
            }

            showResult(answers);

        })
    }
}

function initalize(){
    let contentForQuestion = document.getElementsByClassName("question").item(0) 
    let questions = getQuestions();
    let currentQuestionNumber = document.getElementsByClassName("current-question").item(0)
    let infoQuestions = {"contentQuestion": contentForQuestion,
                        "questions": questions,
                        "currentQuestionNumber": currentQuestionNumber,
                        "index": 0}; 
    loadQuestion(0, infoQuestions);
    return infoQuestions;
}

function loadQuestion(index, infoQuestions){
    if(index >= 0 && index < infoQuestions.questions.length){
        infoQuestions.index = index;
        infoQuestions.currentQuestionNumber.innerHTML = infoQuestions.index+1 + "/" + infoQuestions.questions.length;
        infoQuestions.contentQuestion.innerHTML = infoQuestions.questions[infoQuestions.index];
    }
}

function checkIfEqual(ans, btn){
    let map = {'yes-btn':'T',
               'part-yes-btn': 'C',
               'no-btn': 'N'};
               return map[btn.className] === ans;
}

function visibilityButtons(answer, buttons){
    for(let btn of buttons){
        if(!checkIfEqual(answer, btn)){
            btn.style.opacity = 0.1;
            btn.style.transform = "scale(0.8)";
        }
        else{
            btn.style.opacity = 1;
            btn.style.transform = "scale(1.1)";
        }
    }
}

function updateButtons(buttons, answers, infoQuestions){
    if(answers[infoQuestions.index]){
        visibilityButtons(answers[infoQuestions.index], buttons);
    }
    else {
        for(let btn of buttons){
            btn.style.opacity = 1;
            btn.style.transform = "scale(1)";
        }
    }

}


function changeQuestion(e, infoQuestions, buttons, answers){
    if(e.target.className === "left-arrow"){
        loadQuestion(infoQuestions.index-1, infoQuestions)
    }
    else {
        loadQuestion(infoQuestions.index+1, infoQuestions)
    }
    updateButtons(buttons, answers, infoQuestions);
}



function eventsToArrows(iq, buttons, answers){
    document.getElementsByClassName("left-arrow").item(0).addEventListener('click', (e) => {changeQuestion(e, iq, buttons, answers)})
    document.getElementsByClassName("right-arrow").item(0).addEventListener('click', (e) => {changeQuestion(e, iq, buttons, answers)})
}


function showHelpDesc(infoDesc){
    for(let info of infoDesc){
        info.style.visibility='visible';
        info.style.opacity = 1;
    }

}

function hideHelpDesc(infoDesc){
    for(let info of infoDesc){
        info.style.visibility='hidden';
        info.style.opacity = 0;
    }

}

function eventsToHelp(){
    let infoHelp = document.getElementsByClassName('help-show').item(0);
    let infoDesc = document.getElementsByClassName('info');
    infoHelp.addEventListener('mouseover', () => {
        showHelpDesc(infoDesc);
    })

    infoHelp.addEventListener('mouseout', () => {
        hideHelpDesc(infoDesc);
    })

}

function makeAnswer(btn, htmlButtons, infoQuestions, answers){
    if(btn.className === "yes-btn"){
        answers[infoQuestions.index] = 'T';

    }
    else if(btn.className === "part-yes-btn"){
        answers[infoQuestions.index] = 'C';

    }
    else if(btn.className === "no-btn"){
        answers[infoQuestions.index] = 'N';
    }
    visibilityButtons(answers[infoQuestions.index], htmlButtons);

}

function keyEvent(buttons, answers, iq){
    document.addEventListener('keydown', (e) =>{
        if(e.key === "ArrowRight"){
            loadQuestion(iq.index+1, iq);
            updateButtons(buttons, answers, iq);
        }
        else if(e.key === "ArrowLeft"){
            loadQuestion(iq.index-1, iq);
            updateButtons(buttons, answers, iq);

        }
    })
}

let infoQuestions = initalize();
eventsToHelp();
let answers = [];
let buttons = loadButtons(["yes-btn", "part-yes-btn", "no-btn"], infoQuestions, answers);
eventsToArrows(infoQuestions, buttons, answers);
keyEvent(buttons, answers, infoQuestions);

