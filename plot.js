export function displayPlot(answers) {
  let labels = {
        'A': 'Czujny',
        'B': 'Samotniczy',
        'C': 'Nie-zwyczajny',
        'D': 'Awanturniczy',
        'E': 'Zmienny',
        'F': 'Dramatyczny',
        'G': 'Pewny siebie',
        'H': 'Wrażliwy',
        'I': 'Oddany',
        'J': 'Sumienny',
        'K': 'Wygodny',
        'L': 'Władczy',
        'M': 'Ofiarny',
        'N': 'Poważny',
    }

    let plotAnswers = [];
    for(let [k, v] of Object.entries(answers)){
        plotAnswers.push({y: v, label:labels[k]});
    }

    plotAnswers.sort((a,b) => {
        return ((a.y > b.y) ? -1 : (a.y < b.y) ? 1 : 0);
    })


    var chart = new CanvasJS.Chart("chart-container", {
        axisX:{
            interval: 1,
            labelAngle: -25 
        },
        title:{
          text: "Wykres psychologicznego autoportretu"
        },
        data: [{				
          type:"column",  
          dataPoints:plotAnswers
        }]
      });
      chart.canvas.setAttribute('id', 'chartCanvas');
      chart.render();

      let chartContainer = document.getElementById('chartCanvas');
      let btnImage = document.getElementById('save-btn');
      let dataURL = chartContainer.toDataURL();
      btnImage.style.display = 'block';
      btnImage.addEventListener('click', () => {
        let a = document.createElement('a');
        a.href = dataURL;
        a.download = 'wykres.png';
        a.click();
      })

}
