import {getAnswers} from './answers.js';
import {displayPlot} from './plot.js';


export function showResult(userAnswer){
    let answers = getAnswers();
    let ansCounter = {
        'A': [0, 0, 0],
        'B': [0, 0, 0],
        'C': [0, 0, 0],
        'D': [0, 0, 0],
        'E': [0, 0, 0],
        'F': [0, 0, 0],
        'G': [0, 0, 0],
        'H': [0, 0, 0],
        'I': [0, 0, 0],
        'J': [0, 0, 0],
        'K': [0, 0, 0],
        'L': [0, 0, 0],
        'M': [0, 0, 0],
        'N': [0, 0, 0]
    };
  
    for(let [i, val] of userAnswer.entries()){
        if(val === 'T'){
            if(answers[i+1].length > 1){
                ansCounter[answers[i+1][0]][0]++;
                ansCounter[answers[i+1][2]][0]++;
            }
            else 
                ansCounter[answers[i+1]][0]++;
            
        }
        else if(val === 'C'){
            if(answers[i+1].length > 1){
                ansCounter[answers[i+1][0]][1]++;
                ansCounter[answers[i+1][2]][1]++;
            }
            ansCounter[answers[i+1]][1]++;
        }
    }


     for(const key in ansCounter) {
        if (Object.hasOwnProperty.call(ansCounter, key)) {
            let element = ansCounter[key];
            let sum = element[0]*2 + element[1];
            ansCounter[key] = sum;
            
        }
    }
    displayPlot(ansCounter);

}